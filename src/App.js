import React from 'react';
import './App.css';
import Button from "./component/Button/Button";
import Modal from "./component/Modal/Modal";
import "./theme/styles/index.scss"
class App extends React.Component {
    state = {
        modal: false,

    }

    firstModal = () => {
        this.setState({modal: !this.state.modal, header: "Alfa,Bravo", text: 'TAKOE', color:'red'})
    }
    secondModal = () => {
        this.setState({modal: !this.state.modal, header: "Delta,Oscar", text: 'SOBCEM DPYrOE'})
    }
    closeModal = () => {
        this.setState({modal: !this.state.modal})
    }


    render() {
        return (<div className="App">
                <header className="App-header">
                    <Button onClick={this.firstModal}
                            btnTitle={"OPEN_FIRST"}
                            btnClass={'FIRST_CLASS'}/>
                    <br/>
                    <Button onClick={this.secondModal}
                            btnTitle={"OPEN_SECOND"}
                            btnClass={'SECOND_CLASS'}
                    />
                    {this.state.modal ?
                        <Modal
                            style={this.state.color}
                            text={this.state.text}
                            header={this.state.header}
                            onClick={this.closeModal}/> : null}

                </header>
            </div>
        );
    }


}

export default App;
