import React from 'react';
import './Button.css';

class Button extends React.Component {
    render() {
        return (
            <button id="myBtn" onClick={this.props.onClick} className={this.props.btnClass}>
                {this.props.btnTitle}
            </button>

        )
    }
}

export default Button;
